﻿using UnityEngine;
using System.Collections;
using System;

/**
 * AccelerationSensorManager
 * - Acceleration Sensor management class.
 * autor : Yusuke Fujioka
 * date  : 2014/07/07
 */
public class AccelerationSensorManager : MonoBehaviour 
{
	#region 
	/// <summary>
	/// 自身のインスタンス
	/// </summary>
	private static AccelerationSensorManager m_Instance;
		
	/// <summary>
	/// デバイスの回転角度
	/// </summary>
	private Vector3 m_Direction;
	#endregion

	#region Property
	/// <summary>
	/// インスタンスを	取得する
	/// </summary>
	/// <returns>取得したインスタンス</returns>
	public static AccelerationSensorManager Instance 
	{
		get 
		{
			if (m_Instance == null) 
			{
				m_Instance = (AccelerationSensorManager)FindObjectOfType (typeof(AccelerationSensorManager));
			
				if (m_Instance == null) 
				{
					Debug.Log ("This component is not found.");
				}
			}
			return m_Instance;
		}
	}
	#endregion

	#region Methods
	/// <summary>
	/// 初回のUpdateの前にCallされる処理
	/// </summary>
	void Start () 
	{
		Screen.orientation = ScreenOrientation.LandscapeLeft;
		m_Direction.Set (0, 0, 0);
	}
		
	/// <summary>
	/// 更新処理
	/// </summary>
	void Update () 
	{
		Debug.Log ("Update Called");

		// Input.accelerationは
		// 0.{デバイスの角度}で値を返す。
		// 例えば、デバイスが90°の時返される値は、0.90となる。
		m_Direction.Set (Input.acceleration.x,
		                Input.acceleration.y,
		                Input.acceleration.z);
	}

	/// <summary>
	/// GUIを設定
	/// </summary>
	void OnGUI () 
	{
		string x = System.String.Format( "DirectionX: {0:F2}", m_Direction.x );
		GUI.Label (new Rect (0, 10, 100, 30), x);
	}
	
	/// <summary>
	/// デバイス回転角度をVector3型で取得する
	/// </summary>
	/// <returns>デバイス回転角度(Vector3)</returns>
	public Vector3 GetDirection ()
	{
		return m_Direction;
	}

	/// <summary>
	/// デバイスX軸の回転度の取得
	/// </summary>
	/// <returns>デバイスX軸の回転度</returns>
	public float GetDirectionX ()
	{
		return m_Direction.x * -1;
	}

	/// <summary>
	/// デバイスY軸の回転度の取得
	/// </summary>
	/// <returns>デバイスY軸の回転度</returns>
	public float GetDirectionY () 
	{
		return m_Direction.y * -1;
	}

	/// <summary>
	/// デバイスZ軸の回転度の取得
	/// </summary>
	/// <returns>デバイスZ軸の回転度</returns>
	public float GetDirectionZ () 
	{
		return m_Direction.z * -1;
	}
	#endregion
}

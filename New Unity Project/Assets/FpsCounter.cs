﻿using UnityEngine;
using System.Collections;

/**
 * FpsCounter
 * - Fps utility class.
 * autor : Yusuke Fujioka
 * date  : 2014/07/07
 */
public class FpsCounter : MonoBehaviour {
	
	const float UPDATE_INTERVAL = 1.0f; 	// 更新頻度

	private int m_Frame;					// Frame数
	private float m_Accumulated;			// Frameカウント基準	
	private float m_TimeUntilNextInterval;	// インターバル

	// Use this for initialization
	void Start () {
		m_TimeUntilNextInterval = UPDATE_INTERVAL;
	}
	
	// Update is called once per frame
	void Update () {
		m_TimeUntilNextInterval -= Time.deltaTime;
		m_Accumulated += Time.timeScale / Time.deltaTime;
		++m_Frame;

		if(m_TimeUntilNextInterval <= 0.0f) {
			// FPS を計算する
			float fps = m_Accumulated / m_Frame;
			string format = System.String.Format( "FPS: {0:F2}", fps );
			guiText.text = format;
			
			m_TimeUntilNextInterval = UPDATE_INTERVAL;
			m_Accumulated = 0.0f;
			m_Frame = 0;
		}
	}
}

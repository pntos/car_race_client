# README #

-------------------------------------
Pntos クライアントサイド 
    - 主にマシンの動作、ステージ制作等を行う

開発環境: Unity
開発言語: C#
担当者: 乾、豊田
------------------------------------- 
![みさわ](http://jigokuno.img.jugem.jp/20090522_1213732.gif)
![みさわ](http://jigokuno.img.jugem.jp/20090825_1421390.gif)
![みさわ](http://jigokuno.img.jugem.jp/20091126_1591712.gif)
--------------------------------------
マシン関連仕様

■ 直進(必須)  
・マシンは勝手に直進する

■ ハンドル(必須)  
・端末をハンドルと見立てて、傾ける事でマシンのハンドル操作を行う

■ ニトロ(必須)  
・一定時間加速するシステム  
・ゲージ式である  
・ゲージは時間経過で回復していく

■ パージ(必須)  
・マシンのパーツを切り離し、マシンの軽量化を行うシステム  
・マシンの耐久度を犠牲にする

■ テイクオフ(必須)  
・マシンの重さ、スピードが条件を満たしている場合、離陸(テイクオフ)する事が出来
る  
・カービィのエアライドの様なイメージ(http://www.youtube.com/watch?v=Gzs0J7qe4sM)

■ 耐久度(必須)  
・何らかの動作により減る  
(パージ、他のマシンと衝突したとき、とりあえずこの二つ)

-------------------------------------
ステージ関連仕様  
■ ステージ(必須)  
・terrainによる凹凸やフリーアセットのEasyRoads3Dを使用して道路を作成  
・トンネルは3dsmaxでモデルを作成し、FBX形式で書き出したものを使用  
-------------------------------------